package common

import "time"

const DefaultTimeout = 7200
const DefaultExecTimeout = 1800
const CheckInterval = 3 * time.Second
const NotHealthyCheckInterval = 300
const UpdateInterval = 3 * time.Second
const UpdateRetryInterval = 3 * time.Second
const ReloadConfigInterval = 3
const HealthyChecks = 3
const HealthCheckInterval = 3600
const DefaultWaitForServicesTimeout = 360
const ShutdownTimeout = 360
const DefaultOutputLimit = 4096 // 4MB in kilobytes
const ForceTraceSentInterval = 30 * time.Second
const PreparationRetries = 20
const DefaultGetSourcesAttempts = 10
const DefaultArtifactDownloadAttempts = 10
const DefaultRestoreCacheAttempts = 10
const KubernetesPollInterval = 3
const KubernetesPollTimeout = 180
const AfterScriptTimeout = 6 * time.Minute
const DefaultMetricsServerPort = 9252
const DefaultCacheRequestTimeout = 240
const DefaultNetworkClientTimeout = 60 * time.Minute

var PreparationRetryInterval = 1 * time.Second
